from sqlalchemy import or_
from flask import Flask, render_template, request, url_for, redirect, flash
from flask_sqlalchemy import SQLAlchemy
# Import for Migrations
from flask_migrate import Migrate
import re


app = Flask(__name__)
# app.config['SQLALCHEMY_DATABASE_URI'] ='sqlite:///crud.db' 
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:sharia28@localhost:5432/test'
app.secret_key="secret key"
db = SQLAlchemy(app)
# Settings for migrations
migrate = Migrate(app, db)

def validemail(email):
    regexEmail = r'^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'  
    return True if re.fullmatch(regexEmail,email) else False

def validphone(phone):
    regexPhone = r"(0|91)?[6-9][0-9]{9}"
    return True if re.fullmatch(regexPhone,phone) else False

class Contact(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    email = db.Column(db.String(100), nullable=False)
    phone = db.Column(db.String(100), nullable=False)

    def __init__(self, name, email, phone):
        self.name = name
        self.email = email
        self.phone = phone

@app.route('/',methods=['POST','GET'])
def index():
    all_contacts=Contact.query.all()
    if request.method=='POST' and 'tag' in request.form:
        tag=request.form['tag']
        search="{}%".format(tag)
        search_contact=Contact.query.filter(or_(Contact.name.like(search),Contact.email.like(search)))
        return render_template('index.html',contact=search_contact)
    else:
        return render_template('index.html',contact=all_contacts)

@app.route('/insert',methods=['POST'])
def insert():
    if request.method=='POST':
        name=request.form['name']
        email=request.form['email']
        phone=request.form['phone']
        try:
            if not validemail:
                flash('Enter valid email address!')
                return redirect(url_for('index'))
            elif not validphone:
                flash('Enter valid phone number!')
                return redirect(url_for('index'))
            else:
                my_contact=Contact(name,email,phone)
                db.session.add(my_contact)
                db.session.commit()
                flash('Contact Added Successfully!')
                return redirect(url_for('index'))
        except:
            return 'There was an issue adding your contact!'
    return redirect(url_for('index'))

@app.route('/update', methods=['GET','POST'])
def update():
    if request.method == 'POST':
        my_contact=Contact.query.get_or_404(request.form.get('id'))
        my_contact.name=request.form['name']
        my_contact.email=request.form['email']
        my_contact.phone=request.form['phone']
        try:
            if not validemail:
                flash('Enter valid email address!')
                return redirect(url_for('index'))
            elif not validphone:
                flash('Enter valid phone number!')
                return redirect(url_for('index'))
            else:
                db.session.commit()
                flash('Contact Updated Successfully!')
                return redirect(url_for('index'))
        except:
            return 'There was an issue updating your contact!'
    return redirect(url_for('index'))

@app.route('/delete/<id>', methods=['GET','POST'])
def delete(id):
    my_contact=Contact.query.get_or_404(id)
    try:
        db.session.delete(my_contact)
        db.session.commit()
        flash('Contact Deleted Successfully!')
        return redirect(url_for('index'))
    except:
        return 'There was a problem deleting that contact!'

with app.app_context():
    db.create_all()


if __name__=="__main__":
    app.run(debug=True, port=5001)
